﻿-- Script was generated by Devart dbForge Studio for MySQL, Version 6.0.128.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 02/02/2018 17:59:00
-- Server version: 5.5.5-10.1.29-MariaDB
-- Client version: 4.1

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE p3;

--
-- Definition for table noticias
--
DROP TABLE IF EXISTS noticias;
CREATE TABLE noticias (
  id INT(11) NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(255) DEFAULT NULL,
  texto VARCHAR(255) DEFAULT NULL,
  categoria VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

-- 
-- Dumping data for table noticias
--
INSERT INTO noticias VALUES
(1, 'Noticia1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in justo semper, viverra dui vel, hendrerit nisl. Nam dignissim blandit lectus id scelerisque. Nunc sed est commodo, accumsan dui sed, tempus nibh. Nam sit amet laoreet elit. Nulla vitae v', 'publico'),
(2, 'Noticia2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in justo semper, viverra dui vel, hendrerit nisl. Nam dignissim blandit lectus id scelerisque. Nunc sed est commodo, accumsan dui sed, tempus nibh. Nam sit amet laoreet elit. Nulla vitae v', 'privado'),
(3, 'Noticia3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in justo semper, viverra dui vel, hendrerit nisl. Nam dignissim blandit lectus id scelerisque. Nunc sed est commodo, accumsan dui sed, tempus nibh. Nam sit amet laoreet elit. Nulla vitae v', 'privado'),
(4, 'Noticia4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in justo semper, viverra dui vel, hendrerit nisl. Nam dignissim blandit lectus id scelerisque. Nunc sed est commodo, accumsan dui sed, tempus nibh. Nam sit amet laoreet elit. Nulla vitae v', 'publico'),
(5, 'Noticia5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in justo semper, viverra dui vel, hendrerit nisl. Nam dignissim blandit lectus id scelerisque. Nunc sed est commodo, accumsan dui sed, tempus nibh. Nam sit amet laoreet elit. Nulla vitae v', 'privado');

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;