<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Basic Project Template</h1>
    <br>
</p>


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
	  data/				  contains database
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



INSTALLATION
------------

### Install via Composer

Ejecutar este comando:

~~~
composer create-project silviagabs/p3 carpeta
~~~

Donde pone carpeta, poner el nombre de  la carpeta donde se descargará el paquete



CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=p3',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```
Crear una base de datos llamada p3.
La tabla y los datos de prueba se adjuntan en la carpeta data.

