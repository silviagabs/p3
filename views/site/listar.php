<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'texto',
            [
                'attribute' => 'categoria',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->categoria, ['site/listar', 'categoria' => $model->categoria]);
                }
            ],
        //['class' => 'yii\grid\ActionColumn'],
        ],
        'summary' => false,
    ]);
            
    

    if (is_object($dataProvider_1)) {
        echo '<div class="jumbotron"><h2>NOTICIAS FILTRADAS POR CATEGORÍA</h2>';
        echo GridView::widget([
            'dataProvider' => $dataProvider_1,
            'columns' => [
                'id',
                'titulo',
                'texto',
                'categoria',
            ],
            'summary' => false,
        ]);
    }
   
    ?>
</div>

</div>
